# Demo Web Chat

## Description

- A web chat made by ReactJS and Firebase.
- Support login with google account, chat with any user, send text, image and sticker, update avatar and profile.
- Try at here: https://flutterchatdemo.firebaseapp.com/

## Screenshot

<img src="https://raw.githubusercontent.com/duytq94/reactjs-chat-demo/master/screenshots/ReactJSChatDemo.gif" height="80%" width="80%">

## How to run

- Clone this project
- Run `npm install` to install packages
- Run `npm start` to start the server
